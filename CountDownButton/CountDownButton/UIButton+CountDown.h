//
//  UIButton+CountDown.h
//  CountDownButton
//
//  Created by 王小爽 on 16/1/13.
//  Copyright © 2016年 wxs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CountDown)

/**
 *  倒计时按钮
 *
 *  @param timeLimit       最大倒计时数
 *  @param title           倒计时结束后显示文本
 *  @param normalImage     背景图片
 *  @param countDownImage  倒计时背景图片
 */
- (void)startWithCountTime:(NSInteger)timeLimit
              endWithTitle:(NSString *)title
     normalBackgroundImage:(UIImage *)normalImage
            countDownImage:(UIImage *)countDownImage;

@end
