//
//  ViewController.m
//  CountDownButton
//
//  Created by 王小爽 on 16/1/13.
//  Copyright © 2016年 wxs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.countDownButton];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)countdownAction:(UIButton *)sender
{
    [_countDownButton startWithCountTime:5 endWithTitle:@"获取验证码" normalBackgroundImage:nil countDownImage:nil];
}

- (UIButton *)countDownButton
{
    if (!_countDownButton) {
        _countDownButton = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 100, 30)];
        [_countDownButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownButton setBackgroundColor:[UIColor greenColor]];
        [_countDownButton addTarget:self action:@selector(countdownAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _countDownButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
