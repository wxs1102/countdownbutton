//
//  ViewController.h
//  CountDownButton
//
//  Created by 王小爽 on 16/1/13.
//  Copyright © 2016年 wxs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+CountDown.h"

@interface ViewController : UIViewController


@property (nonatomic, strong) UIButton *countDownButton;

@end

