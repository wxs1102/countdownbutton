//
//  main.m
//  CountDownButton
//
//  Created by 王小爽 on 16/1/13.
//  Copyright © 2016年 wxs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
