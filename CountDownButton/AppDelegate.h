//
//  AppDelegate.h
//  CountDownButton
//
//  Created by 王小爽 on 16/1/13.
//  Copyright © 2016年 wxs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

